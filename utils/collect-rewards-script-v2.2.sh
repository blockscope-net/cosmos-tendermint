#!/bin/bash
# Script autodelegate node rewards (by blockscope.net)

echo "Enter password:"
read -s password


CLI="fetchd"
RPC_PORT=26657
TOKEN="afet"

## NOTE: set correct the index of your wallet in jq command (pipe)
WALLET_NAME=$(echo -e "${password}" | ${CLI} keys list --output json | jq -r .[0].name)
WALLET_ADDRESS=$(echo -e "${password}" | ${CLI} keys list --output json | jq -r .[0].address)
VALOPER=$(echo -e "${password}" | ${CLI} keys show ${WALLET_NAME} --bech val --output json | jq -r .address)
CHAIN_ID=$(curl -s http://localhost:${RPC_PORT}/status | jq -r '.result.node_info.network')

INTERVAL=86400
MAX_BLOCK_TIME=20
FEE=5000
REMAINING=1000000000000000000
MIN_REWARDS_TO_DELEGATE=1000000000000000000

DELEGATE_TO_EXTERNAL=false
EXTERNAL_WALLET_ADDRESS=""


while true;
do
        date_time=$(date '+%d/%m/%Y %H:%M:%S')
        echo -e "\n${date_time} :: Withdrawing all rewards..."
        
        # ADD fees to transactions if needed by the network -»  --fees ${FEE}${TOKEN}

        echo -e "${password}" | ${CLI} tx distribution withdraw-all-rewards --from ${WALLET_NAME} --chain-id ${CHAIN_ID} -y --node tcp://localhost:${RPC_PORT}
        sleep $MAX_BLOCK_TIME

        echo -e "${password}" | ${CLI} tx distribution withdraw-rewards $VALOPER --from ${WALLET_NAME} --commission --chain-id ${CHAIN_ID} -y --node tcp://localhost:${RPC_PORT}                
        sleep $MAX_BLOCK_TIME

        AMOUNT=$(${CLI} query bank balances ${WALLET_ADDRESS} --node tcp://localhost:${RPC_PORT} --chain-id ${CHAIN_ID} --output=json | jq -r '.balances[].amount')
        DELEGATE_AMOUNT=$(bc <<< "$AMOUNT - $REMAINING")
        echo -e "Account balance: ${AMOUNT}${TOKEN} - Amount to delegate: ${DELEGATE_AMOUNT}${TOKEN} - Min amount to delegate: ${MIN_REWARDS_TO_DELEGATE}${TOKEN}"


        if (( $(echo "$DELEGATE_AMOUNT > $MIN_REWARDS_TO_DELEGATE" | bc) ))
        then
                if [ $DELEGATE_TO_EXTERNAL == true ]
                then
                        echo -e "Delegating to external account ${EXTERNAL_WALLET_ADDRESS}..."
                        echo -e "${password}" | ${CLI} tx bank send ${WALLET_NAME} ${EXTERNAL_WALLET_ADDRESS} ${DELEGATE_AMOUNT}${TOKEN} --chain-id ${CHAIN_ID} --node tcp://localhost:${RPC_PORT} -y
                else
                        echo -e "Delegating rewards to node..."     
                        echo -e "${password}" | ${CLI} tx staking delegate ${VALOPER} ${DELEGATE_AMOUNT}${TOKEN} --from ${WALLET_NAME}  --chain-id ${CHAIN_ID} --node tcp://localhost:${RPC_PORT} -y
                fi
        else
                echo -e "Not enough balance to delegate! ($DELEGATE_AMOUNT < $MIN_REWARDS_TO_DELEGATE)"
        fi

        sleep $INTERVAL
done

