#!/bin/bash
# Script rescue rewards

echo "Enter password:"
read -s password


CHAIN_ID="umeevengers-1c"
EXTERNAL_RPC_NODE="tcp://65.21.196.90:26657"


WALLET_ADDRESS_3B2="umee1css9jzaeu2aera62tgfgyks78z0zm90v42zwtd"
WALLET_ADDRESS_3B3="umee1xq4avgrpfql63glxrvztvmg00leyhqkh6wlauk"


WALLET_ADDRESS_SOURCE=${WALLET_ADDRESS_3B2}
WALLET_ADDRESS_TARGET=${WALLET_ADDRESS_3B3}


BROADCAST_MODE="async"  # Modes: block, sync, async

INTERVAL=0.01
FEE=200
GAS=20000
REMAINING=20000


while true;
do
        date_time=$(date '+%d/%m/%Y %H:%M:%S')
        echo -e "${date_time} :: Withdrawing funds..."

        AMOUNT=$(umeed query bank balances ${WALLET_ADDRESS_SOURCE} --node tcp://localhost:26657 --chain-id ${CHAIN_ID} --output=json | jq -r '.balances[].amount')
        AMOUNT=$(bc <<< "$AMOUNT - 1 - $FEE - $FEE - $GAS")

        if (( $(echo "$AMOUNT > $REMAINING" | bc) )); then
                echo -e "Sending from ${WALLET_ADDRESS_SOURCE} to ${WALLET_ADDRESS_TARGET} (amount: 1 of ${AMOUNT} uumee)..."
                echo -e "${password}" | umeed tx bank send ${WALLET_ADDRESS_SOURCE} ${WALLET_ADDRESS_TARGET} 1uumee --chain-id ${CHAIN_ID} --broadcast-mode ${BROADCAST_MODE} --node ${INTERNAL_RPC_NODE} --note "tx script" -y --gas auto --gas-adjustment 1.4 --gas-prices 0.01uumee
        else
                echo -e "Not enough balance to transfer! ($AMOUNT < $REMAINING)"
        fi

        sleep $INTERVAL
done


