#!/bin/bash
# Script send fixed number of transactions
echo "Enter password:"
read -s password


CLI="umeed"
RPC_PORT=26657
TOKEN="uumee"

CHAIN_ID=$(curl -s http://localhost:${RPC_PORT}/status | jq -r '.result.node_info.network')

INTERVAL=5
AMOUNT=1
BRIDGE_FEE=1
REMAINING=100000
#GAS_ADJUSTMENT=1.5
TRANSFER_TYPE="cosmos2eth"          # [ cosmos | cosmos2eth | eth2cosmos ]

# Accounts
FROM_NAME=test

FROM="umee1...."          # test
TO="umee1....2"            # test2

FROM_ETH="0x..."       # test
TO_COSMOS="umee1...."     # test

FROM_COSMOS="umee1...."   # test
TO_ETH="0x..."         # test


ETH_PK_CLEAN=""
ETH_RPC="https://goerli-light.nodes.guru"
ETH_UMEE_TOKEN_ADDRESS="0xe54fbaecc50731AFe54924C40dfD1274f718fe02"


TX_ID=1
TX_NUMBER=5
TX_FAILED=0


while [ $TX_ID -le $TX_NUMBER ] do
    date_time=$(date '+%d/%m/%Y %H:%M:%S')
    echo -e "\n${date_time} :: Sending TX (${TX_ID})..."

    BALANCE=$(${CLI} query bank balances ${FROM} --node tcp://localhost:${RPC_PORT} --chain-id ${CHAIN_ID} --output=json | jq -r '.balances[].amount')
    MIN_BALANCE=$(bc <<< "$AMOUNT + $BRIDGE_FEE + $REMAINING")

    if (( $(echo "$BALANCE > $MIN_BALANCE" | bc) ))
    then
        echo -e "Sending ${TRANSFER_TYPE} from ${WALLET_ADDRESS_SOURCE} to ${WALLET_ADDRESS_TARGET} (amount: 1 of ${AMOUNT} uumee)..."

        if [[ "$TRANSFER_TYPE" == "cosmos" ]]; then
            echo -e "${password}" | ${CLI} tx bank send ${FROM_NAME} ${TO} ${AMOUNT}${TOKEN} --chain-id ${CHAIN_ID} -y
        elif [[ "$TRANSFER_TYPE" == "cosmos2eth" ]]; then
            echo -e "${password}" | ${CLI} tx peggy send-to-eth ${TO} ${TX_ID}${TOKEN} ${BRIDGE_FEE}${TOKEN} --from ${FROM_NAME} --fee-account ${FROM} --chain-id ${CHAIN_ID} -y
        elif [[ "$TRANSFER_TYPE" == "eth2cosmos" ]]; then
            peggo bridge send-to-cosmos ${ETH_UMEE_TOKEN_ADDRESS} ${TO} ${TX_ID} --eth-pk=${ETH_PK_CLEAN} --eth-rpc=${ETH_RPC}
        else
            #exit 1
            echo "error"
        fi

        TX_ID=$(($TX_ID-1))
    else
        echo -e "Not enough balance to send! ($BALANCE < $MIN_BALANCE)"
    fi

    sleep $INTERVAL
done

echo -e "Summary: succeded=${TX_ID} - failed=${TX_FAILED}"
