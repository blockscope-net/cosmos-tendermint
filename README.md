# Cosmos Tendermint

Utilities, docs and programs for Cosmos projects

## Utilities

### Auto delegation script

Bash script to collect rewards and auto delegate on your validator, or send to an external account for safe funds storage.

## Docs

## License

For open source projects, say how it is licensed.
